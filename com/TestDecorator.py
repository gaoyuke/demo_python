# -*- coding: utf-8 -*-


def log(f):
    def fn(*args):
        print 'call ' + f.__name__ + '()...'
        return f(*args)
    return fn


# 阶乘
@log
def factorial(n):
    return reduce(lambda x, y: x * y, range(1, n + 1))


print factorial(10)


@log
def add(x, y):
    return x + y


print add(1, 2)
