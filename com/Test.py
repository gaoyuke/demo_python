# -*- coding: utf-8 -*-

t = (1, 2, 'a', ['b', 'c'])
temp = t[3]
temp[0] = 'd'
temp[1] = 'e'
print t


# 计算n的阶乘
def fact(n):
    if n == 1:
        return 1
    return n * fact(n - 1)


fact(10)


# 汉诺塔
def move(n, a, b, c):
    if n == 1:
        print a, '-->', c
        return
    move(n - 1, a, c, b)
    print a, '-->', c
    move(n - 1, b, a, c)


move(4, 'A', 'B', 'C')


# 计算x的n次方，默认为计算平方
def power(x, n=2):
    s = 1
    while n > 0:
        n = n - 1
        s = s * x
    return s


power(5)
power(2, 10)
