# -*- coding: utf-8 -*-


def f(l):
    def g():
        return sum(l)

    return g


x = f([1, 2, 3, 4, 5])
print x  # x是一个方法
print x()  # 这是x方法执行的结果


def calc_prod(lst):
    def lazy_prod():
        def multiply(x, y):
            return x * y

        return reduce(multiply, lst)

    return lazy_prod


temp = calc_prod([1, 2, 3, 4])
print temp()


def count():
    fs = []
    for i in range(1, 4):
        def tmp(i):
            return lambda: i * i

        fs.append(tmp(i))
    return fs


f1, f2, f3 = count()
print f1(), f2(), f3()
map(lambda x: x * x, [1, 2, 3, 4])

sorted([2, 1, 3, 6, 4, 5], lambda x, y: -cmp(x, y))  # 匿名函数实现list由大到小排序


def f(x):
    if x <= 10:
        return '(-∞, 10]'
    elif 10 < x <= 20:
        return '(10, 20]'
    else:
        return '(20, +∞)'


print f(5) + ', ' + f(15) + ', ' + f(25)
print filter(lambda s: s and len(s.strip()) > 0, ['test', None, '', 'str', '  ', 'END'])
